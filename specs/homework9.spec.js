import { describe, test } from '@jest/globals';
import { apiProvider } from '../framework';
import { MailBuilder } from '../framework/builder/user';

test('Positive test', async () => {
  const demo = new MailBuilder();
  const params = demo
    .addAccessKey()
    .addMail('fixed')
    .generate();

  const { body, status } = await apiProvider().check().get(params);
  expect(status).toBe(200);
  expect(body.format_valid).toBeTruthy();
});

test('Positive test for random mail', async () => {
  const demo = new MailBuilder();
  const params = demo
    .addAccessKey()
    .addMail('random')
    .generate();
  const { body, status } = await apiProvider().check().get(params);
  expect(status).toBe(200);
  expect(body.format_valid).toBeTruthy();
});

test('Cheking access without api key', async () => {
  const demo = new MailBuilder();
  const params = demo
    .addAccessKey('null')
    .addMail('random')
    .generate();
  const { body, status } = await apiProvider().checkNoKey().get(params);
  expect(status).toBe(200);
  expect(body.error.code).toBe(101);
  expect(body.error.type).toBe('missing_access_key');
});

describe('Negative tests', () => {
  test.each`
      key                          | mail                          | expected
      ${false}                     | ${' '}                        | ${210}
      ${'b614becf2c67e5c960'}      | ${'test@mail.ru'}             | ${101}
      ${' '}                       | ${'test@mail.ru'}             | ${101}
      ${false}                     | ${'test@mail.ru&catch_all=1'} | ${310}
   `('Validating $key and $mail ', async ({ key, mail, expected }) => {
    const demo = new MailBuilder();
    const params = demo
      .addAccessKey(key)
      .addMail(mail)
      .generate();
    const r = await apiProvider().check().get(params);
    expect(r.body.error.code).toBe(expected);
  });
});
