import { MailCheck, MailCheckNoKey } from './services/index';

const apiProvider = () => ({
  check: () => new MailCheck(),
  checkNoKey: () => new MailCheckNoKey(),
});

export { apiProvider };
