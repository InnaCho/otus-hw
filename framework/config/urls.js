const urls = {
  vikunja: 'https://try.vikunja.io',
  booker: 'https://restful-booker.herokuapp.com',
  demo: 'https://91i.ru',
  mailboxlayer: 'https://apilayer.net',
};
export { urls };
