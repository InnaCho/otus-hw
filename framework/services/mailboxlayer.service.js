import supertest from 'supertest';
import { urls } from '../config';

const MailCheck = function MailCheck() {
  this.get = async function mailCheck(params) {
    const r = await supertest(urls.mailboxlayer)
      .get(`/api/check?access_key=${params.access_key}&email=${params.mail}`);
    return r;
  };
};

export { MailCheck };

const MailCheckNoKey = function MailCheckNoKey() {
  this.get = async function mailCheckNoKey(params) {
    const r = await supertest(urls.mailboxlayer)
      .get(`/api/check?email=${params.mail}`);
    return r;
  };
};

export { MailCheckNoKey };
