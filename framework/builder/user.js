import faker from 'faker';
import { mailParams } from '../config/index';

function deepCopy(example) {
  return JSON.parse(JSON.stringify(example));
}
const BuildUser = function () {
  // eslint-disable-next-line consistent-return
  this.get = function (type) {
    // eslint-disable-next-line default-case
    switch (type) {
      case 'new':
        // eslint-disable-next-line no-case-declarations
        const data = {
          login: faker.internet.email(),
          password: faker.vehicle.vin(),
          birth_date: '2020-12-07T17:09:14.344Z',
          nick: 'string22',
        };
        return deepCopy(data);
    }
  };
};

export { BuildUser };

const BuildEmail = function () {
  this.get = function (type) {
    switch (type) {
      case 'fixed':
        return 'chonkainna@mail.ru';
      case 'random':
        return faker.internet.email();
      default:
        return 'defaultmail@gmail.com';
    }
  };
};

export { BuildEmail };

const MailBuilder = function MailBuilder() {
  this.addAccessKey = function addAccessKey(key) {
    if (key) {
      this.access_key = key;
      return this;
    }
    this.access_key = mailParams.access_key;
    return this;
  };
  this.addMail = function addMail(type) {
    switch (type) {
      case 'fixed':
        this.mail = 'chonkainna@mail.ru';
        return this;
      case 'random':
        this.mail = faker.internet.email();
        return this;
      default:
        this.mail = type;
        return this;
    }
  };
  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    const data = {};
    fields.forEach((fieldName) => {
      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    return data;
  };
};

export { MailBuilder };
